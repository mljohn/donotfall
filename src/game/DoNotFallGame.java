/**
 * File: DoNotFallGame
 * Author: Michelle John
 * Date: 16 December 2018
 * Purpose: Create a game
 */
package game;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.font.BitmapText;
import com.jme3.font.LineWrapMode;
import com.jme3.font.Rectangle;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Main class. Extends {@link SimpleApplication} and implements {@link ActionListener}
 * and {@link PhysicsCollisionListener}.
 */
public class DoNotFallGame extends SimpleApplication implements ActionListener, PhysicsCollisionListener {

    private BulletAppState bulletAppState;
    private CharacterControl playerControl;
    private Spatial blob;
    private Node blobNode;
    
    private final List<Spatial> targetBlobs = new ArrayList<>();

    private final Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false;
    
    private final Vector3f camDir = new Vector3f();
    private final Vector3f camLeft = new Vector3f();
    
    private String scoreText;
    private int score;
    private String instructionText;
    BitmapText scoreBoard;
    
    private static final String RESET = "Reset";
    private static final String JUMP = "Jump";
    private static final String DOWN = "Down";
    private static final String UP = "Up";
    private static final String RIGHT = "Right";
    private static final String LEFT = "Left";
    

    /**
     * Entry into program.
     * 
     * @param args the arguements to start the program with
     */
    public static void main(String[] args) {
        DoNotFallGame app = new DoNotFallGame();
        app.start();
    }

    @Override
    public void simpleInitApp() {

        // Application setup
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        setUpKeys();
        
        // Scene setup
        Spatial sceneModel = assetManager.loadModel("Scenes/manyLights/Main.scene");
        sceneModel.setLocalTranslation(0, -2, 0);
        sceneModel.setLocalScale(0.25f);
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(sceneModel);
        RigidBodyControl sceneControl = new RigidBodyControl(sceneShape, 0f);
        sceneModel.addControl(sceneControl);

        // Player setup
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(0.5f, 0.5f, 1);
        playerControl = new CharacterControl(capsuleShape, 0.05f);
        playerControl.setJumpSpeed(20);
        playerControl.setFallSpeed(30);
        playerControl.setGravity(new Vector3f(0, -30f, 0));
        playerControl.setPhysicsLocation(new Vector3f(0, 0, 0)); // Center pillar
        
        // Blob setup
        Material sphereMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        sphereMat.setColor("Color", ColorRGBA.Blue);
        blobNode = new Node();
        // Creates a sphere at every pillar and adds it to a list. 
        List<Vector3f> pillarLocationList = populatePillarLocations();
        for (int i = 0; i < pillarLocationList.size(); i++) {
            Sphere s = new Sphere(24, 24, 0.25f);
            Spatial sphere = new Geometry("sphere_" + i, s);
            sphere.setMaterial(sphereMat);
            
            sphere.setLocalTranslation(pillarLocationList.get(i));
            RigidBodyControl sphereControl = new RigidBodyControl(new SphereCollisionShape(0.25f), 0);
            sphere.addControl(sphereControl);
            targetBlobs.add(sphere);
        }       
        
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
        
        addBlob();
        
        // Scoreboard setup
        guiNode.detachAllChildren();
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        scoreText = "Score: ";
        instructionText = "W = forwards    S = backwards\nA = left\t   D = right\nR = reset\tSpacebar = jump\n";
        scoreBoard = new BitmapText(guiFont, false);
        scoreBoard.setSize(guiFont.getCharSet().getRenderedSize());
        scoreBoard.setText(instructionText + scoreText);
        scoreBoard.setBox(new Rectangle(0, 65, 450, 65));
        scoreBoard.setLineWrapMode(LineWrapMode.Word);
        scoreBoard.setLocalTranslation(200, scoreBoard.getLineHeight(), 0);
        guiNode.attachChild(scoreBoard);
        
        // Game setup
        rootNode.attachChild(sceneModel);
        rootNode.attachChild(blobNode);
        bulletAppState.getPhysicsSpace().add(sceneModel);
        bulletAppState.getPhysicsSpace().add(playerControl);
    }

    /**
     * Sets the keys to use for this game. 
     * Key "A" moves the player to the left.
     * Key "D" moves the player to the right.
     * Key "W" moves the player forward.
     * Key "S" moves the player backwards.
     * Spacebar makes the player jump
     * Key "R" resets the game.
     */
    private void setUpKeys() {
        inputManager.addMapping(LEFT, new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping(RIGHT, new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping(UP, new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping(DOWN, new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping(JUMP, new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping(RESET, new KeyTrigger(KeyInput.KEY_R));
        
        inputManager.addListener(this, LEFT, RIGHT, UP, DOWN, JUMP, RESET);
    }
    
    /**
     * Populates the list with the coordinates of the tops of the pillars in the scene.
     * Creates {@link Vector3f} objects for the tops of each of the pillars in the scene. 
     * Each loop iterates 5 times.
     * 
     * @returns the populated list of vectors
     */
    private List<Vector3f> populatePillarLocations() {
        List<Vector3f> pillarLocationList = new ArrayList<>();
        // The centers of the pillars are approximately 4 units apart.
        for (int x = -8; x <= 8; x += 4) {
            for (int z = -8; z <= 8; z += 4) {
                pillarLocationList.add(new Vector3f(x, 0, z));
            }
        }
        return pillarLocationList;
    }

    @Override
    public void simpleUpdate(float tpf) {
        // If the player falls off the pillars, the score is reset to 0.
        if (playerControl.getPhysicsLocation().y < 0) score = 0;
        scoreText = "Score: " + score;
        scoreBoard.setText(instructionText + scoreText);
        
        camDir.set(cam.getDirection()).multLocal(0.15f);
        camLeft.set(cam.getLeft()).multLocal(0.1f);
        walkDirection.set(0, 0, 0);
        if (left) walkDirection.addLocal(camLeft);
        if (right) walkDirection.addLocal(camLeft.negate());
        if (up) walkDirection.addLocal(camDir);
        if (down) walkDirection.addLocal(camDir.negate());
        playerControl.setWalkDirection(walkDirection);
        walkDirection.y = 0;
        playerControl.setWalkDirection(walkDirection.normalize().mult(0.1f));
        cam.setLocation(playerControl.getPhysicsLocation());
    }

    @Override
    public void simpleRender(RenderManager rm) {
        // Unused method.
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        switch (name) {
            case LEFT:
                left = isPressed;
                break;
            case RIGHT:
                right = isPressed;
                break;
            case UP:
                up = isPressed;
                break;
            case DOWN:
                down = isPressed;
                break;
            case JUMP:
                // prevents double jumping while the player is on the pillars
                if (isPressed && playerControl.getPhysicsLocation().y < 1) 
                    playerControl.jump(new Vector3f(0, 10, 0));
                break;
            case RESET:
                if (isPressed) playerControl.setPhysicsLocation(new Vector3f(0, 0, 0));
                break;
            default:
                break;
        }
    }

    @Override
    public void collision(PhysicsCollisionEvent event) {
        boolean hit = false;
        if (event.getNodeA() != null && event.getNodeA().getName().startsWith("sphere")) hit = true;
        if (event.getNodeB() != null && event.getNodeB().getName().startsWith("sphere")) hit = true;
        if (hit) {
            bulletAppState.getPhysicsSpace().remove(blob);
            blobNode.detachChild(blob);

            addBlob();

            score++;
            System.out.println(score);
        }
    }
    
    /**
     * Adds a randomly selected blob to the game.
     */
    private void addBlob() {
        blob = targetBlobs.get(ThreadLocalRandom.current().nextInt(0, targetBlobs.size()));
        bulletAppState.getPhysicsSpace().add(blob);
        blobNode.attachChild(blob);
    }
}
